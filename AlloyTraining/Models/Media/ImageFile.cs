﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace AlloyTraining.Models.Media
{
    [ContentType(DisplayName = "ImageFile", GUID = "bb05d0f5-f116-44c8-aa4f-4e04f98a85ef", Description = "")]
    public class ImageFile : ImageData
    {
        public virtual String Description { get; set; } 
    }
}